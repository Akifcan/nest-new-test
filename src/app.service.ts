import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello() {
    return { hello: 'world' };
  }

  getCode() {
    return 150;
  }

  addPost() {
    return 150;
  }
}
