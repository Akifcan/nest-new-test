import { Controller, Get, Inject } from '@nestjs/common';
import { PostService } from './post.service';

@Controller('post')
export class PostController {
  @Inject() private readonly postService: PostService;

  @Get('/list-posts')
  posts() {
    return this.postService.listAllPosts();
  }
}
