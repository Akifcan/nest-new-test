import { HttpService } from '@nestjs/axios';
import { Inject, Injectable } from '@nestjs/common';
import axios from 'axios';

@Injectable()
export class PostService {
  @Inject() private readonly httpService: HttpService;

  async listAllPosts() {
    const res = await axios.get('https://jsonplaceholder.typicode.com/posts');
    return res.data;
  }
}
