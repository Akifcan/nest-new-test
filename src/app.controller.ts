import { Controller, Get, HttpCode, Post, Req } from '@nestjs/common';
import { Request } from 'express';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello() {
    return this.appService.getHello();
  }

  @Post('/get')
  getCode() {
    return this.appService.getCode();
  }

  @Post('/add')
  @HttpCode(201)
  add(@Req() req: Request) {
    console.log(req.body);
    return 'ok';
  }
}
