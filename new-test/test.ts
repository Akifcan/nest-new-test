import { execSync } from 'child_process';
import { readdirSync, readFileSync, unlinkSync, writeFileSync } from 'fs';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const newTest = require('./newtest.config.json');
// eslint-disable-next-line @typescript-eslint/no-var-requires
const { name, version, description } = require('../package.json');

(async () => {
  const { testRegex } = newTest;

  const percentage = (percent: number, total: number) => {
    return (100 * percent) / total;
  };

  const writeToFile = async (
    testedRoutes: { path: string; isTested: boolean }[],
    totalEndpoint: number,
    testedEndpoint: number,
    coverage: number,
  ) => {
    console.log('write');

    const date = new Date();
    const testedResultHtml = testedRoutes
      .map((x) => {
        return `
            <tr>
              <td>${x.path}</td>
              <td>${x.isTested ? ' ✅' : '❌'} </td>
            </tr>
    `;
      })
      .join('');

    writeFileSync(
      `./results/${date.toISOString()}-${date.getTime()}.html`,
      `
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>${date.toISOString()} - ${date.toTimeString()} - Tests Results</title>

    <style>
      * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        font-family: sans-serif;
      }

      body {
        padding: 2rem;
      }

      hr {
        margin-block: 1rem;
      }

      table {
        width: 100%;
      }

      th,
      td {
        border: 1px solid #006994;
        padding: 0.5rem;
        text-align: start;
      }
    </style>
  </head>
  <body>
    <h1>${name} - project</h1>
    <hr />
    <ul>
      <li><b>Version:</b> ${version}</li>
      <li><b>Description:</b> ${description}</li>
    </ul>
    <hr />
    <h1>Total Endpoints: ${totalEndpoint}</h1>
    <h2 style="margin-block: 1.5rem">Tested Endpoints: ${testedEndpoint}</h2>
    <h2 style="margin-block-end: 1.5rem">Coverage: ${coverage}</h2>
    
    <table>
      <thead>
        <th>
          End Point
        </th>
        <th>
          Is Tested
        </th>
      </thead>
      <tbody>
        ${testedResultHtml}
      </tbody>
    </table>

  </body>
</html>


`,
    );
    console.log('coverage generated!');
  };

  const files = readdirSync('./').filter((file) => file.endsWith(testRegex));

  for (const i in files) {
    const result = execSync(`ts-node ${files[i]}`).toString();
    console.log(result);
    console.log(`${files[i]} done ✅`);
  }

  const registeredRoutes = readFileSync(
    './test-info/registered-routes.txt',
  ).toString();
  const testedRoutes = readFileSync('./test-info/tested-routes.txt').toString();

  unlinkSync('./test-info/registered-routes.txt');
  unlinkSync('./test-info/tested-routes.txt');

  const registeredRoutesArr = registeredRoutes.split(',').slice(0, -1);
  const testedRoutesArr = testedRoutes.split(',').slice(0, -1);

  const routes = registeredRoutesArr.map((r) => {
    if (testedRoutesArr.includes(r)) {
      return {
        path: r,
        isTested: true,
      };
    } else {
      return {
        path: r,
        isTested: false,
      };
    }
  });

  writeToFile(
    routes,
    registeredRoutesArr.length,
    testedRoutesArr.length,
    percentage(testedRoutesArr.length, registeredRoutesArr.length),
  );
})();
