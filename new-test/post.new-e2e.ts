#!/usr/bin/env ts-node-script
import { HttpModule } from '@nestjs/axios';
import { PostController } from '../src/post/post.controller';
import { PostService } from '../src/post/post.service';
import { NewTest, expect, Request } from './core';

NewTest.create(
  {
    imports: [HttpModule],
    providers: [PostService],
    controllers: [PostController],
  },
  () => {
    console.log('before all');
  },
).then(async (test) => {
  test.run(
    [
      test.it('/list-posts list posts', async () => {
        const result = await Request.get('/post/list-posts');
        expect(result.statusCode).toBe(200);
      }),
    ],
    () => console.log('after all extended!'),
  );
});
