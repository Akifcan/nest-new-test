#!/usr/bin/env ts-node-script
import { AppController } from '../src/app.controller';
import { AppService } from '../src/app.service';
import { NewTest, expect, Request } from './core';

NewTest.create(
  {
    imports: [],
    controllers: [AppController],
    providers: [AppService],
  },
  () => {
    console.log('before all');
  },
).then(async (test) => {
  test.run(
    [
      test.it('/get endpoint', async () => {
        const result = await Request.post('/get');
        expect(result.statusCode).toBe(201);
      }),
      test.it('/ endpoint', async () => {
        const result = await Request.get('/');
        expect(result.statusCode).toBe(200);
      }),
      test.it('/add endpoint', async () => {
        const result = await Request.post('/add', { name: 'sebnem ferah' });
        expect(result.statusCode).toBe(201);
      }),
    ],
    () => console.log('after all extended!'),
  );
});
