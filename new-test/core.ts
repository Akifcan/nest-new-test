import { INestApplication, Module, ModuleMetadata } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import axios from 'axios';
import { existsSync, writeFileSync, appendFileSync } from 'fs';

const LCERROR = '\x1b[31m%s\x1b[0m'; //red
const LCWARN = '\x1b[33m%s\x1b[0m'; //yellow
const LCINFO = '\x1b[36m%s\x1b[0m'; //cyan
const LCSUCCESS = '\x1b[32m%s\x1b[0m'; //green

const lines = '-------------------';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const { name, version, description } = require('../package.json');

function percentage(percent: number, total: number) {
  return (100 * percent) / total;
}

const writeToFile = async (
  testResult: { result: boolean; label: string; description: string }[],
  testedRoutes: { path: string; isTested: boolean }[],
  totalEndpoint: number,
  testedEndpoint: number,
  coverage: number,
) => {
  console.log('write');

  const date = new Date();
  const testedResultHtml = testedRoutes
    .map((x) => {
      return `
            <tr>
              <td>${x.path}</td>
              <td>${x.isTested ? ' ✅' : '❌'} </td>
            </tr>
    `;
    })
    .join('');

  const resultHtml = testResult
    .map((x) => {
      return `
      <tr>
          <td>${x.result ? '✅' : '❌'}</td>
          <td>${x.description}</td>
          <td>${x.label}</td>
        </tr>

    `;
    })
    .join('');
  writeFileSync(
    `./results/${date.toISOString()}-${date.getTime()}.html`,
    `
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>${date.toISOString()} - ${date.toTimeString()} - Tests Results</title>

    <style>
      * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        font-family: sans-serif;
      }

      body {
        padding: 2rem;
      }

      hr {
        margin-block: 1rem;
      }

      table {
        width: 100%;
      }

      th,
      td {
        border: 1px solid #006994;
        padding: 0.5rem;
        text-align: start;
      }
    </style>
  </head>
  <body>
    <h1>${name} - project</h1>
    <hr />
    <ul>
      <li><b>Version:</b> ${version}</li>
      <li><b>Description:</b> ${description}</li>
    </ul>
    <hr />
    <table>
      <thead>
        <th>Result</th>
        <th>Description</th>
        <th>Label</th>
      </thead>
      <tbody>
        ${resultHtml}
      </tbody>
    </table>

    <hr />
    <h1>Total Endpoints: ${totalEndpoint}</h1>
    <h2 style="margin-block: 1.5rem">Tested Endpoints: ${testedEndpoint}</h2>
    <h2 style="margin-block-end: 1.5rem">Coverage: ${coverage}</h2>
    
    <table>
      <thead>
        <th>
          End Point
        </th>
        <th>
          Is Tested
        </th>
      </thead>
      <tbody>
        ${testedResultHtml}
      </tbody>
    </table>

  </body>
</html>


`,
  );
  console.log('coverage generated!');
};

const writeRegisteredRoutes = (routes: any) => {
  if (existsSync('./test-info')) {
    appendFileSync('./test-info/registered-routes.txt', routes + ',');
  } else {
    writeFileSync('./test-info/registered-routes.txt', routes);
  }
};

const writeTestedRoutes = (routes: any) => {
  if (existsSync('./test-info')) {
    appendFileSync('./test-info/tested-routes.txt', routes + ',');
  } else {
    writeFileSync('./test-info/tested-routes.txt', routes);
  }
};

async function bootstrap(appModule: ModuleMetadata) {
  @Module(appModule)
  class AppModule {}

  const app = await NestFactory.create(AppModule);
  await app.listen(3000);
  console.log('Testing App is Up');
  return app;
}

export class NewTest {
  descriptions: string[] = [];
  static app: INestApplication;
  static testedRoutes: string[] = [];
  static registeredRoutes: string[] = [];
  static exceptIndexes: { result: boolean; label: string }[] = [];

  public static async create(
    appModule: ModuleMetadata,
    beforeAllFn?: () => void,
  ): Promise<NewTest> {
    const test = new NewTest();
    this.app = await test.runApp(appModule);
    const server = this.app.getHttpServer();
    const router = server._events.request._router.stack;
    NewTest.registeredRoutes = router
      .map((x: any) => {
        return x.route ? x.route.path : undefined;
      })
      .filter((y: any) => y !== undefined);
    console.log(NewTest.registeredRoutes);

    beforeAllFn();
    return test;
  }

  async run(tests: any[], afterAllFn?: () => void) {
    for (const i in tests) {
      await tests[i];
    }
    afterAllFn();
    this.afterAll();
  }

  private constructor() {
    // ...
  }

  private async runApp(appModule: ModuleMetadata): Promise<INestApplication> {
    return await bootstrap(appModule);
  }

  async it(description: string, fn: () => any) {
    await fn();
    this.descriptions.push(description);
  }

  afterAll() {
    const results = NewTest.exceptIndexes.map((test, index) => {
      return { description: this.descriptions[index], ...test };
    });
    console.log(LCWARN);
    console.log(results);
    const successCount: number = results.filter(
      (x) => x.result === true,
    ).length;
    const failedCount: number = results.filter(
      (x) => x.result === false,
    ).length;
    console.log(lines);
    console.log(LCINFO, 'Results \n', LCINFO);
    console.log(
      LCSUCCESS,
      'Success Count ----',
      LCERROR,
      'Failed Count',
      '---- \n',
      LCSUCCESS,
      '\t' + successCount,
      '\t \t',
      LCERROR,
      failedCount,
    );
    console.log(lines);
    const routes = NewTest.registeredRoutes.map((r) => {
      if (NewTest.testedRoutes.includes(r)) {
        return {
          path: r,
          isTested: true,
        };
      } else {
        return {
          path: r,
          isTested: false,
        };
      }
    });
    writeRegisteredRoutes(NewTest.registeredRoutes.join(','));
    writeTestedRoutes(NewTest.testedRoutes.join(','));
    // writeToFile(
    //   results,
    //   routes.filter((a, i) => routes.findIndex((s) => a.path === s.path) === i),
    //   routes.filter((a, i) => routes.findIndex((s) => a.path === s.path) === i)
    //     .length,
    //   NewTest.testedRoutes.length,
    //   percentage(NewTest.testedRoutes.length, NewTest.registeredRoutes.length),
    // );
    if (failedCount > 0) {
      throw new Error('Test failed');
    }
    process.exit();
  }
}

export class Request {
  static async get(url: string) {
    try {
      const result = await axios.get(`http://localhost:3000${url}`);
      NewTest.testedRoutes.push(url);
      return {
        statusCode: result.status,
        body: result.data,
        statusText: result.statusText,
      };
    } catch (e) {
      return {
        statusCode: 404,
        body: 'not found uri',
        statusText: 'not found',
      };
    }
  }
  static async post(url: string, body: Record<string, any> = {}) {
    try {
      const result = await axios.post(`http://localhost:3000${url}`, {
        body,
      });
      NewTest.testedRoutes.push(url);
      return {
        statusCode: result.status,
        body: result.data,
        statusText: result.statusText,
      };
    } catch (e) {
      return {
        statusCode: 404,
        body: 'not found uri',
        statusText: 'not found',
      };
    }
  }
}

export const expect = (value: any) => {
  const toBe = (control: any) => {
    if (value === control) {
      NewTest.exceptIndexes.push({
        result: true,
        label: `✅ Test is success. Received: ${value} - Excepted: ${control}`,
      });
    }
    if (value !== control) {
      NewTest.exceptIndexes.push({
        result: false,
        label: `❌ Test is success. Received: ${value} - Excepted: ${control}`,
      });
    }
  };
  const toGreaterThan = (control: any) => {
    if (value >= control) {
      NewTest.exceptIndexes.push({
        result: true,
        label: `✅ Test is success. Received: ${value} - Excepted: ${control}`,
      });
    }
    if (value != control) {
      NewTest.exceptIndexes.push({
        result: false,
        label: `❌ Test is success. Received: ${value} - Excepted: ${control}`,
      });
    }
  };

  return {
    toBe,
    toGreaterThan,
  };
};
